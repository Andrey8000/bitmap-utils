## This is a library for downloading image and image-processing in android, with samples.
### Module library - library with classes for working with bitmaps and image-loading with disk caching.
### Module sample - sample of usage.
______________________________________________________________________________________________________
## How to use QueueImageLoader
### Simple usage (all context-linked objects will be automatically converted in to WeakReferences and it optimized for fast-scrolling ListView or RecyclerView):
```
QueueImageLoader.justLoadInToImageView(this, imageView, "http://your_image_url", 0);
```