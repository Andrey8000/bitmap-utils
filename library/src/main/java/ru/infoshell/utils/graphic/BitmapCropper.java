package ru.infoshell.utils.graphic;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Helper for bitmap cropping
 *
 * <br><br>Created by Andrey Riyk
 */
public class BitmapCropper {
	
	/**
	 * Get cropped bitmap box
	 */
	public static Bitmap getCroppedBitmapBox(Bitmap source, int maxSize) {
		
		if (source == null) return null;

		final int sourceWidth = source.getWidth();
		final int sourceHeight = source.getHeight();
		
		int size = sourceHeight > sourceWidth? sourceWidth : sourceHeight;
		if (size>maxSize) size = maxSize;
		
		int 	left = sourceWidth/2 - size/2
				, top = sourceHeight/2 - size/2
				, bottom = sourceHeight/2 + size/2
				, right = sourceWidth/2 + size/2 ;
		
		
		final int outputWidth =  right - left;
		final int outputHeight = bottom - top;
		
		if (sourceWidth < outputWidth || sourceHeight < outputHeight) {
			return source;
		}
		
		final Bitmap output = Bitmap.createBitmap(outputWidth, outputHeight, Bitmap.Config.RGB_565);
		
		final Rect dest = new Rect(0, 0, outputWidth, outputHeight);
		
		final Rect src = new Rect(left, top, right, bottom);
		
		Canvas canvas = new Canvas(output);
		canvas.drawColor(Color.WHITE);
		canvas.drawBitmap(source, src, dest, new Paint());
		
		return output;
	}
	
}
