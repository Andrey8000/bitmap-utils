package ru.infoshell.utils.graphic.network;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.infoshell.utils.graphic.BitmapResizeHelper;
import ru.infoshell.utils.graphic.BitmapSave;
import ru.infoshell.utils.helpers.Lo;


/**
 * Simple image loader with thread pool
 *
 * <br><br>Created by Andrey Riyk
 */
//TODO how about RAM-memory cache? (Lru from v4 or self-written)
public class QueueImageLoader {

    /** Default thread count in executor (if not invoke init() method) */
    private static final int DEFAULT_TREAD_CNT = 2;

    /** When queue too big - it can mean trouble */
    private static final int QUEUE_DANGER_SIZE = 20;

    /** Main executor for image loading */
    private static ExecutorService executor;



    /** Main tasks map with key-url and ImageLoadingCallback */
    private static final ConcurrentHashMap<String, ImageLoadingCallback> taskList =
            new ConcurrentHashMap<String, ImageLoadingCallback>();

    /** Map for checking ImageView already in loading task (actual for ListView and RecyclerView) */
    private static final ConcurrentHashMap<String, WeakReference<ImageView>> imageCallbacks =
            new ConcurrentHashMap<String, WeakReference<ImageView>>();

    /** Map with HttpURLConnections to abort them if need */
    private static final ConcurrentHashMap<String, HttpURLConnection> activeConnections =
            new ConcurrentHashMap<String, HttpURLConnection>();

    /** Map with BufferedInputStreams to abort them if need */
    private static final ConcurrentHashMap<String, BufferedInputStream> activeInputStreams =
            new ConcurrentHashMap<String, BufferedInputStream>();


    /** Suffix for file-cache directory */
    private static final String CACHE_SUB_DIR = "/images_cache_12/";

    /** Path to file-cache directory */
    private static String cacheDir;

    /** Default valid time for file cache in mills */
    private static final long DEFAULT_FILE_CACHE_VALID_TIME = 10 * 60 * 1000;

    /** Current valid time for file cache in mills */
    private static long fileCacheValidTime = DEFAULT_FILE_CACHE_VALID_TIME;

    /** Maximum file name length */
    private static final int MAX_FILE_NAME_LENGTH = 120;

    /** Maximum cache files size - default value */
    private static final int MAX_FILE_CACHE_SIZE_DEFAULT = 30 * 1024 * 1024;

    /** Maximum cache files size - current value */
    private static int maxFileCacheSize = MAX_FILE_CACHE_SIZE_DEFAULT;

    /** Last calculated cache files size */
    private static volatile long lastFileCacheSize;

    /** Logs-enable flag */
    private static boolean logEnabled = false;



    /**
     * Initialization with give thread count. If not invoke in code, will be invoked automatically
     */
    public static void init(int threadsCount){
        executor = Executors.newFixedThreadPool(threadsCount);
    }



    /**
     * Enable or disable logs
     */
    public static void setLogEnabled(boolean logEnabled) {
        QueueImageLoader.logEnabled = logEnabled;
    }



    /**
     * Set actual lifetime for file cache
     */
    @SuppressWarnings("unused")
    public static void setFileCacheValidTime(long timeInMills){
        fileCacheValidTime = timeInMills;
    }



    /**
     * Set current cache files size
     */
    @SuppressWarnings("unused")
    public static void setFileCacheMaxSize(int sizeInBytes){
        maxFileCacheSize = sizeInBytes;
    }



    /**
     * Add task to load (in queue) with custom callback
     * @param url url to load image
     * @param callback callback
     * @param overrideIfExist true, if need to override existing task with given url
     * @param tryFromCache try to get image from file cache (usually need false)
     * @param tryCount retry count
     * @param maxSide max image side (for resize) 0 - no resize
     * @param additionalWork runnable-similar interface to do work with bitmap in thread, before it apply to ImageView (may be null).
     *                       Note: you must avoid create this class anonymous to avoid ths.-memory leaks
     */
    @Deprecated
    public static synchronized void addTask(String url, final ImageLoadingCallback callback, boolean overrideIfExist,
                                            boolean tryFromCache, final int tryCount, int maxSide, final AdditionalWork additionalWork){
        if (executor==null || executor.isShutdown()) init(DEFAULT_TREAD_CNT);
        if (!taskList.containsKey(url)) {
            Runnable runnable = getNewRunnableForTask(url, tryFromCache, tryCount, maxSide, additionalWork);
            taskList.put(url, callback);
            executor.submit(runnable);
        }
        else if (overrideIfExist) {
            taskList.put(url, callback);
        }

        if (taskList.size()>QUEUE_DANGER_SIZE)
            logError("QueueImageLoader taskList.size(): "+taskList.size());
    }



    /**
     * Simple loading image in to ImageView with optional image resizing, caching and callback.
     * Also suitable for ListView and RecyclerView.
     * This method contains WeakReferences and checking for reload for one ImageView.
     * @param activity activity for runOnUiThread (wrap in to WeakReference)
     * @param imageView ImageView to applying loaded image (wrap in to WeakReference)
     * @param url url to load image
     * @param maxSide max image side (for resize) 0 - no resize
     */
    public static synchronized void justLoadInToImageView(Activity activity, ImageView imageView, final String url, int maxSide){
        justLoadInToImageView(activity, imageView, url, 2, maxSide, null, null);
    }

    /**
     * Simple loading image in to ImageView with optional image resizing, caching and callback.
     * Also suitable for ListView and RecyclerView.
     * This method contains WeakReferences and checking for reload for one ImageView.
     * @param activity activity for runOnUiThread (wrap in to WeakReference)
     * @param imageView ImageView to applying loaded image (wrap in to WeakReference)
     * @param url url to load image
     * @param tryCount retry count
     * @param maxSide max image side (for resize) 0 - no resize
     * @param callback custom callback (may be null)
     * @param additionalWork runnable-similar interface to do work with bitmap in thread, before it apply to ImageView (may be null).
     *                       Note: you must avoid create this class anonymous to avoid ths.-memory leaks
     */
    public static synchronized void justLoadInToImageView(Activity activity, ImageView imageView, final String url,
                                                          int tryCount, int maxSide, final CallbackWithImageView callback, final AdditionalWork additionalWork){
        if (executor==null || executor.isShutdown()) init(DEFAULT_TREAD_CNT);
        log("QueueImageLoader justLoadInToImageView:  " + url + "   taskList: "+taskList.size());

        if (TextUtils.isEmpty(cacheDir) && activity!=null) {
            cacheDir = activity.getCacheDir() + CACHE_SUB_DIR;
            log("QueueImageLoader init cacheDir: " + cacheDir);
            File cacheDirFile = new File(cacheDir);
            if (!cacheDirFile.exists()) {
                boolean make = cacheDirFile.mkdir();
                if (!make) logError("Error on creating cache directory!");
            }
        }

        boolean tryFromCache = isFileValidByTime(BitmapSave.getFileLastModify(getFullPathToSaveFile(url)));

        final WeakReference<ImageView> weakReference = new WeakReference<ImageView>(imageView);
        if (isImageViewInWork(imageView)) {
            String lastUrlInThisImageView = getUrlInImageCallbacksByImageView(imageView);
        //    Lo.w("lastUrlInThisImageView "+lastUrlInThisImageView);
            taskList.remove(lastUrlInThisImageView);
            imageCallbacks.remove(lastUrlInThisImageView);
            stopLoadingStreamAndConnection(lastUrlInThisImageView);
        }
        imageCallbacks.put(url, weakReference);

        final WeakReference<Activity> weakActivity = new WeakReference<Activity>(activity);

        addTask(url, new QueueImageLoader.ImageLoadingCallback() {
            @Override
            public void onReady(final String info, final Bitmap bitmap) {
                log("QueueImageLoader justLoadInToImageView onReady :  " + url + "   taskList: "+taskList.size());
                final ImageView iv = weakReference.get();
                if (iv!=null) {
                    Activity activity = weakActivity.get();
                    if (activity!=null){
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                iv.setImageBitmap(bitmap);
                                if (callback!=null) callback.onReady(info, bitmap, weakReference);
                            }
                        });
                    }
                }
                imageCallbacks.remove(url);
            }

            @Override
            public void onError(final String info) {
                imageCallbacks.remove(url);
                if (callback!=null) callback.onError(info);
                logError("QueueImageLoader justLoadInToImageView onError: " + info);
            }
        }, true, tryFromCache, tryCount, maxSide, additionalWork);
    }



    /**
     * Delete task from queue by url
     */
    @SuppressWarnings("unused")
    public static void delFromTaskList(String url){
        if (taskList.containsKey(url))
            taskList.remove(url);
    }



    /**
     * Clear all tasks in queue
     */
    @SuppressWarnings("unused")
    public static void clearTasks() {
        taskList.clear();
        if (executor != null  && !executor.isShutdown()) {
            executor.shutdownNow();
            init(DEFAULT_TREAD_CNT);
        }
    }



    /**
     * Stop executor with threads
     */
    @SuppressWarnings("unused")
    public static void StopExecutor() {
        taskList.clear();
        if (executor != null  && !executor.isShutdown()) {
            executor.shutdownNow();
        }
    }



    /**
     * Get file name to save from url (replacing bad-characters and length-control)
     */
    public static String getFileNameFromUrl(String url){
        if (url==null) return "null";
        String result = url.replaceAll("/", "");
        if (result.length()>MAX_FILE_NAME_LENGTH) {
    //        Lo.i("getRightPartOfUrl big length "+result);
            result = result.substring(result.length() - MAX_FILE_NAME_LENGTH, result.length());
        }
    //    Lo.i("\n getRightPartOfUrl: "+url + " \n -> "+result );
        return result;
    }



    private static boolean isFileValidByTime(long fileLastModifyTime){
        return System.currentTimeMillis() < fileLastModifyTime + fileCacheValidTime;
    }



    private static boolean isImageViewInWork(ImageView imageView){
        for (WeakReference<ImageView> weakReference : imageCallbacks.values()){
            if (weakReference.get()!=null && weakReference.get().equals(imageView)) return true;
        }
        return false;
    }



    private static String getFullPathToSaveFile(String url){
        if (TextUtils.isEmpty(cacheDir))
            logError("cacheDir empty!");
        return cacheDir + getFileNameFromUrl(url);
    }



    private static String getUrlInImageCallbacksByImageView(ImageView imageView){
        for (String url : imageCallbacks.keySet()){
            WeakReference<ImageView> weakReference = imageCallbacks.get(url);
            if (weakReference.get()!=null && weakReference.get().equals(imageView)) return url;
        }
        return null;
    }



    private static Runnable getNewRunnableForTask(final String url, final boolean fromCache, final int tryCount, final int maxSide,
                                                  final AdditionalWork additionalWork){
        return new Runnable() {
                @Override
                public void run() {
                    if (taskList.containsKey(url)) {
                        final LoadingResult result = fromCache? getImageFromFile(url, true, tryCount, maxSide) : downloadImage(url, true, tryCount, new int[1], maxSide);
                        if (additionalWork!=null) result.bitmap = additionalWork.work(result.bitmap);
                        if (taskList.containsKey(url)) {
                            synchronized (QueueImageLoader.class) {
                                ImageLoadingCallback callback = taskList.get(url);
                                if (!result.error) {
                                    callback.onReady(result.info, result.bitmap);
                                } else {
                                    callback.onError(result.info);
                                }
                                taskList.remove(url);
                            }
                        }
                        result.clear();
                    }
                }
            };
    }



    private static void log(String string){
        if (logEnabled) Lo.d(string);
    }



    private static void logError(String string){
        Lo.e(string);
    }



    //TODO check correctly work
    private static void stopLoadingStreamAndConnection(String url){
        if (activeConnections.contains(url)){
            HttpURLConnection connection = activeConnections.get(url);
            try {
                connection.disconnect();
            } catch (Exception e) {
                logError("stopLoading connection "+e);
            }
            activeConnections.remove(url);
        }
        if (activeInputStreams.contains(url)){
            BufferedInputStream bufferedInputStream = activeInputStreams.get(url);
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                logError("stopLoading stream "+e);
            }
            activeInputStreams.remove(url);
        }
    }



    //TODO may be convert filesMap to class variable to optimization work?
    private static synchronized void checkAndTrimCacheFilesSize(long maxSize){
        if (TextUtils.isEmpty(cacheDir)) return;
        File directory = new File(cacheDir);
        if (!directory.exists() || !directory.isDirectory()) return;

        long maxSizeWithReserve = (long) (maxSize*0.75f); // deleting with the reserve for more rarely call this method
        File[] files = directory.listFiles();
        TreeMap<Long, File> filesMap = new TreeMap<Long, File>();
        long totalSize = 0;
        for (File file : files) {
            if (file.isFile()) {
                totalSize += file.length();
                filesMap.put(file.lastModified(), file);
            }
        }
        log("checkAndTrimCacheFilesSize totalSize: "+totalSize+ "    filesMap: "+filesMap.size());
        lastFileCacheSize = totalSize;
        if (totalSize > maxSize){
            for(Long key : filesMap.keySet()){
                File currFile = filesMap.get(key);
                long size = currFile.length();
                log("key to del: "+key + "   size: "+size);
                String fileName = currFile.getName();
                boolean del = currFile.delete();
                if (del) {
                    totalSize -= size;
                    lastFileCacheSize = totalSize;
                    if (totalSize < maxSizeWithReserve)
                        return;
                }
                else
                    logError("delete cache file error: "+fileName);
            }
        }
    }



    private static LoadingResult downloadImage(String url, boolean tryFromDiskOnError, int maxTryCnt, int[] currTryCnt, int maxSide){
        log("QueueImageLoader downloadImage begin... currTryCnt: " + currTryCnt[0]+" / " + maxTryCnt + "    url: "+url);

        if (!url.contains("http")) return new LoadingResult(null, LoadingResult.NOT_VALID_URL, true);

        if (maxTryCnt<1) maxTryCnt = 1;

        boolean error = false;
        LoadingResult loadingResult = null;
        HttpURLConnection conn = null;
        BufferedInputStream bufStream = null;
        try {
            conn = (HttpURLConnection) new URL(url).openConnection();
            activeConnections.put(url, conn);
            conn.setDoInput(true);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.connect();

            InputStream is = conn.getInputStream();
            bufStream = new BufferedInputStream(is, 8192);
            activeInputStreams.put(url, bufStream);

            String filePath = getFullPathToSaveFile(url);
            Bitmap bitmap = BitmapFactory.decodeStream(bufStream);
            BitmapSave.saveToDisk(bitmap, filePath, Bitmap.CompressFormat.PNG, 85);

            bufStream.close();
            conn.disconnect();
            bufStream = null;
            conn = null;

            if (maxSide>0){
                bitmap.recycle();
                bitmap = BitmapResizeHelper.decodeSampledBitmapFromDisk(filePath, maxSide, maxSide, true);
            }


            if (lastFileCacheSize==0 )
                checkAndTrimCacheFilesSize(maxFileCacheSize);
            File imageFile = new File(filePath);
            if (imageFile.exists()) {
                long fileSize = imageFile.length();
                lastFileCacheSize += fileSize;
            }
            if (lastFileCacheSize > maxFileCacheSize)
                checkAndTrimCacheFilesSize(maxFileCacheSize);


            error = bitmap == null;
            loadingResult = new LoadingResult(bitmap, LoadingResult.RESULT_OK, false);
            log("QueueImageLoader downloadImage ok " + url);

        } catch (Exception e) {
            logError("QueueImageLoader downloadImage:  " + e);
            error = true;
            e.printStackTrace();
            loadingResult = new LoadingResult(null, e+"", true);
        } finally {
            if ( bufStream != null ) try { bufStream.close(); } catch (IOException ex) {/*no need*/}
            if ( conn != null )  conn.disconnect();
            activeInputStreams.remove(url);
            activeConnections.remove(url);
        }

        //TODO in this realization this recursion will block thread, until retry attempts will not end. May be paste extra check for available this url in task list?
        currTryCnt[0]++;
        if (error && currTryCnt[0]<maxTryCnt){
            try { Thread.sleep(500); } catch (InterruptedException e) {/* no need */}
            loadingResult = downloadImage(url, tryFromDiskOnError, maxTryCnt, currTryCnt, maxSide);
        }
        else if (error && tryFromDiskOnError)
            loadingResult = getImageFromFile(url, false, 0, maxSide);

        return loadingResult;
    }



    private static LoadingResult getImageFromFile(String url, boolean allowLoadIfError, final int tryCountFromWeb, int maxSide){
        log("QueueImageLoader getImageFromFile begin... " + getFullPathToSaveFile(url));

        LoadingResult loadingResult = null;
        boolean error = false;
        try {
            Bitmap bitmap = maxSide>0?
                    BitmapResizeHelper.decodeSampledBitmapFromDisk(getFullPathToSaveFile(url), maxSide, maxSide, true)
                    : BitmapFactory.decodeFile(getFullPathToSaveFile(url));
            error = bitmap == null;
            log("QueueImageLoader getImageFromFile error: " + error + "  bitmap: "+bitmap);
            if (!error) loadingResult = new LoadingResult(bitmap, LoadingResult.RESULT_OK, false);
        }catch(Exception e){
            loadingResult = new LoadingResult(null, e+"", true);
            error = true;
            e.printStackTrace();
        }

        if (error && allowLoadIfError)
            loadingResult = downloadImage(url, false, tryCountFromWeb, new int[1], maxSide);
        else if (error && loadingResult==null)
            loadingResult = new LoadingResult(null, "unknown error in getImageFromFile", true);

        return loadingResult;
    }



    public static class LoadingResult{

        public static final String RESULT_OK = "ok";
        public static final String NOT_VALID_URL = "url is not valid";
        Bitmap bitmap;
        String info;
        boolean error;

        private LoadingResult(Bitmap bitmap, String info, boolean error) {
            this.bitmap = bitmap;
            this.info = info;
            this.error = error;
        }

        public void clear(){
            bitmap = null;
            info = null;
        }
    }



    public static interface ImageLoadingCallback{
        public void onReady(String info, Bitmap bitmap);
        public void onError(String info);
    }



    public static interface CallbackWithImageView {
        public void onReady(String info, Bitmap bitmap, WeakReference<ImageView> weakImageView);
        public void onError(String info);
    }



    public static interface AdditionalWork{
        public Bitmap work(Bitmap bitmap);
    }
}
