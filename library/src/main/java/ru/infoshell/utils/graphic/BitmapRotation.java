package ru.infoshell.utils.graphic;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.IOException;

import ru.infoshell.utils.helpers.Lo;

/**
 * Helper for bitmap rotations bitmap-ов
 *
 * <br><br>Created by Andrey Riyk
 */
public class BitmapRotation {


    /**
     * Get rotation of image from disk
     */
    public static int getRotation(String path){
        try {
            final ExifInterface exif = new ExifInterface(path);
            final int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90;
            }
        } catch (IOException e) {
            Lo.e("error in getting orientation: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return 0;
    }



    /**
     * Rotate bitmap for given degrees
     */
    public static Bitmap rotate(Bitmap bitmap, int degrees) {
        final Matrix matrix = new Matrix();
        matrix.postRotate(degrees);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }


}
