package ru.infoshell.utils.helpers;

/**
 * Хэлпер для замера времени выполнения участков кода
 *
 * <br><br>Created by Andrey Riyk
 */
public class TimerMeter {
	
	public static final String LOG = "TIMER: ";
	
	private long start;
	private String info;
	
	public TimerMeter(String info){
		start = System.currentTimeMillis();
		this.info = info;
	}
	
	public long end(){
		long totalTime = System.currentTimeMillis() - start;
		Lo.w(LOG + info + "  time: " + totalTime);
		return totalTime;
	}

    public long end(String extraInfo){
        long totalTime = System.currentTimeMillis() - start;
        Lo.w( LOG + info+ "( "+extraInfo + ")  time: " + totalTime);
        return totalTime;
    }

}
