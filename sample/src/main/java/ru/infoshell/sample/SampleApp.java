package ru.infoshell.sample;

import android.app.Application;

import ru.infoshell.utils.helpers.Lo;

/**
 * Created by Andrey Riyk on 11.11.2014.
 */
public class SampleApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Lo.init(getApplicationContext().getPackageName());
        Lo.i("SampleApp onCreate");
    }

}
