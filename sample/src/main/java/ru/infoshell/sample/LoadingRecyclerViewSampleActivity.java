package ru.infoshell.sample;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import ru.infoshell.utils.graphic.BitmapCropper;
import ru.infoshell.utils.graphic.BitmapResizeHelper;
import ru.infoshell.utils.graphic.BitmapRounder;
import ru.infoshell.utils.graphic.BitmapTextCreator;
import ru.infoshell.utils.graphic.network.QueueImageLoader;
import ru.infoshell.utils.helpers.Lo;


/**
 * Sample activity with RecyclerView
 */
public class LoadingRecyclerViewSampleActivity extends ContextLeakTestActivity {

    // test data
    public static final List<String> imagesUrl = new ArrayList<String>();
    static{
        imagesUrl.add("http://playcast.ru/uploads/2013/06/07/5505854.jpg");
        imagesUrl.add("http://www.resimle.net/data/media/58/Subaru%20yaris.jpg");
        imagesUrl.add("http://www.poetryclub.com.ua/upload/poem_all/00175475.jpg");
        imagesUrl.add("http://www.yugopolis.ru/data/mediadb/2383/0000/0099/9944.jpg");
        imagesUrl.add("http://s60.radikal.ru/i168/0901/fd/fbd37c2176c1.jpg");
        imagesUrl.add("http://www.ankawa.com/forum/index.php?PHPSESSID=skk0tgk9druchhkpbg4ahqbck1&action=dlattach;topic=477204.0;attach=793478;image");
        imagesUrl.add("http://gigasoft.pp.ua/_nw/55/61992038.jpeg");
        imagesUrl.add("http://webryba.ru/gallery/fqnuh4iw0ss9.jpg");
        imagesUrl.add("http://www.mb-portal.net/gallery/d/2097-2/C216+02.jpg");
        imagesUrl.add("http://im2-tub-ru.yandex.net/i?id=3ea9b5d4f7fac711885a1a5c794aa490-128-144&n=21");
        imagesUrl.add("http://www.wwalls.ru/large/201210/23022.jpg");
        imagesUrl.add("http://i688.photobucket.com/albums/vv244/crankx_2009/cloud.jpg");
        imagesUrl.add("http://i019.radikal.ru/1308/14/6ad65b6abd35.jpg");
        imagesUrl.add("http://f12.ifotki.info/org/ec1eb349f0b57e1fa4a8d65609911b1a2e7421138518614.jpg");
        imagesUrl.add("http://s1.blomedia.pl/gadzetomania.pl/images/2011/09/largestsolarsail.jpg");
        imagesUrl.add("http://www.bugaga.ru/uploads/posts/2009-03/1237155381_00221_connecticutriver_1920x1200.jpg");
        imagesUrl.add("http://vsetke.ru/thumbnails/7d6/1f18af3bda5f431077c48a4fe396b28a.jpg");
        imagesUrl.add("http://rsm.haber365.com/N/25_inanilmaz_doga_olaylari%20(34).jpg");
        imagesUrl.add("http://www.infotech1s.com/images/2010/08/Fun-Moody-Skies-3_0000.jpg");
        imagesUrl.add("http://i015.radikal.ru/1104/9c/e5c1d1a1a751.jpg");
        imagesUrl.add("http://n.foto.radikal.ru/0701/e0958912cfb6.jpg");
        imagesUrl.add("http://luxfon.com/large/201203/9244.jpg");
        imagesUrl.add("http://www.coolwallpapers.org/wallpapers/4/461/thumb/600_vol93-025.jpg");
        imagesUrl.add("http://img0.liveinternet.ru/images/attach/c/8/99/711/99711522_large_Nature_Flowers_Beautiful_Tulips_032955_29.jpg");
        imagesUrl.add("http://img0.liveinternet.ru/images/attach/c/7/98/942/98942626_large_53253.jpg");
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_loading);

        getSupportActionBar().setTitle("RecyclerView sample");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(linearLayoutManager);

        MyAdapter adapter = new MyAdapter(this, imagesUrl, getResources().getDimensionPixelSize(R.dimen.image_size_small));
        recyclerView.setAdapter(adapter);

    }



    private static class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private List<String> items = new ArrayList<String>();
        private WeakReference<Activity> weakActivity;
        private int imageSize;

        public MyAdapter(Activity activity, List<String> items, int imageSize){
            super();
            this.items = items;
            weakActivity = new WeakReference<Activity>(activity);
            this.imageSize = imageSize;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            viewHolder.text.setText("item url: (" + i + ")  " + items.get(i));
            viewHolder.imageView.setImageDrawable(null);
            viewHolder.imageView.clearAnimation();
            QueueImageLoader.justLoadInToImageView(weakActivity.get(), viewHolder.imageView, items.get(i), 2, imageSize, new QueueImageLoader.CallbackWithImageView() {
                @Override
                public void onReady(String info, Bitmap bitmap,  WeakReference<ImageView> weakImageView) {
                    ImageView imageView = weakImageView.get();
                    if (imageView!=null){
                        Animation animation = AnimationUtils.loadAnimation(imageView.getContext(), R.anim.anim_transparent_to_visible);
                        imageView.startAnimation(animation);
                    }
                }

                @Override
                public void onError(String info) {

                }
            },
            new AdditionalWorker()
            );
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {
            TextView text;
            ImageView imageView;
            public ViewHolder(View itemView) {
                super(itemView);
                text = (TextView) itemView.findViewById(R.id.textView);
                imageView = (ImageView) itemView.findViewById(R.id.imageView);
            }
        }
    }


    /** static class for avoid this.-memory leak in activity */
    private static class AdditionalWorker implements QueueImageLoader.AdditionalWork{

        @Override
        public Bitmap work(Bitmap bitmap) {
            return BitmapRounder.round(bitmap, 0.15f, Bitmap.Config.ARGB_8888);
        }
    }

}
